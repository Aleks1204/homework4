<?php
#Написать программу, которая выводит простые числа
for ($i = 1; $i <= 100; $i++) {
    if ($i != 2 && $i % 2 == 0) continue;
    if ($i != 3 && $i % 3 == 0) continue;
    if ($i != 5 && $i % 5 == 0) continue;
    if ($i != 7 && $i % 7 == 0) continue;
    echo $i . " - ";
}
echo '<hr>';

# Сгенерируйте 100 раз новое число и выведите на экран количество четных чисел из этих 100.
for ($i = 1; $i <= 100; $i++) {
    $ran = rand(1, 10);
    if ($ran % 2 == 0) {
        $arr[] = $ran;
    }
}
foreach ($arr as $key => $value);
echo $key;
echo '<hr>';

#Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько раз сгенерировались эти числа (1, 2, 3, 4 и 5).
$one = $two = $three = $four = $five = 0;
for ($i = 1; $i <= 100; $i++) {
    $ran = rand(1, 5);
    if ($ran == 1) {
        $one++;
    } elseif ($ran == 2) {
        $two++;
    } elseif ($ran == 3) {
        $three++;
    } elseif ($ran == 4) {
        $four++;
    } elseif ($ran == 5) {
        $five++;
    }
}
echo "1 сгенерировалась $one раз";
echo "<br>";
echo "2 сгенерировалась $two раз";
echo "<br>";
echo "3 сгенерировалась $three раз";
echo "<br>";
echo "4 сгенерировалась $four раз";
echo "<br>";
echo "5 сгенерировалась $five раз";
echo '<hr>';

#Используя условия и циклы сделать таблицу в 5 колонок и 3 строки (5x3), отметить разными цветами часть ячеек.
echo "<table border = 1>";
for ($l = 0; $l < 3; $l++) {
    echo "<tr>";
    for ($i = 0; $i < 5; $i++) {
        if ($i % 2) {
            echo '<td style="background-color:aqua">', "aqua", "</td>";
        } else {
            echo '<td style="background-color:yellow">', "yellow", "</td>";
        }
    }
    echo "</tr>";
}

echo "</table>";
echo '<hr>';

#В переменной month лежит какое-то число из интервала от 1 до 12. Определите в какую пору года попадает этот месяц
echo $month = rand(1, 12) . "<br>";

if ($month > 2 && $month < 6) {
    echo "Весна";
} elseif ($month > 5 && $month < 9) {
    echo "Лето";
} elseif ($month > 8 && $month < 12) {
    echo "Осень";
} else {
    echo "Зима";
}
echo '<hr>';

#Дана строка, состоящая из символов, например, 'abcde'. Проверьте, что первым символом этой строки является буква 'a'.
$st = "abcde";

if ($st[0] === "a") {
    echo "Да";
} else {
    echo "Нет";
}
echo '<hr>';

#Дана строка с цифрами, например, '12345'. Проверьте, что первым символом этой строки является цифра 1, 2 или 3. 
$st = '22345';
if ($st[0] == 1) {
    echo "Да";
} elseif ($st[0] == 2) {
    echo "Да";
} elseif ($st[0] == 3) {
    echo "Да";
} else {
    echo "Нет";
}

echo '<hr>';

#Если переменная test равна true, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при test, равном true, false. - тернарка и if else.

$test = true;
if ($test) {
    echo 'Верно';
} else {
    echo 'Неверно';
}

echo ($test) ? 'Верно' : 'Неверно';
echo '<hr>';

#Если переменная lang = ru вывести массив на русском языке, а если en то вывести на английском языке. Сделат через if else и через тернарку
$rus = [1 => 'пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$eng = [1 => 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

$lang = $eng;

if ($lang === $rus) {
    echo "<pre>";
    print_r($rus);
    echo "</pre>";
} elseif ($lang === $eng) {
    echo "<pre>";
    print_r($eng);
    echo "</pre>";
} else {
    echo "нет такого языка";
}

echo ($lang === $rus) ? print_r($rus) : print_r($eng);
echo '<hr>';

#Определите в какую четверть часа попадает это число
echo $time = rand(0, 59) . "<br>";

if ($time <= 15) {
    echo "Первая четверть";
} elseif ($time > 15 && $time < 31) {
    echo "Вторая четверть";
} elseif ($time >= 31 && $time < 46) {
    echo "Третья четверть";
} elseif ($time >= 46) {
    echo "Четвертая четверть";
}
echo "<br>";
echo $time <= 15 ? "Первая четверть" : ($time > 15 && $time < 31 ? "Вторая четверть" : ($time >= 31 && $time < 46 ? "Третья четверть" : "Четвертая четверть"));
echo '<hr>';

#Развернуть этот массив в обратном направлении
#for and foreach
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
foreach ($arr as $key => $value) {
}
$key;
for ($i = $key; $i > -1; $i--) {
    print_r($arr[$i] . " ; ");
}
echo '<hr>';

#while
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
foreach ($arr as $key => $value) {
}
$key += 1;
while ($key > 0) {
    $key--;
    print_r($arr[$key] . " : ");
}
echo '<hr>';

#do-while
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
foreach ($arr as $key => $value) {
}
$key += 1;
do {
    $key--;
    print_r($arr[$key] . " : ");
} while ($key > 0);
echo '<hr>';

#1.8
#for and foreach
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
foreach ($arr as $key => $value) {
    $count++;
}
for ($i = $count; $i > -1; $i--) {
    print_r($arr[$i] . " : ");
}
echo '<hr>';

#while
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
foreach ($arr as $key => $value) {
    $count++;
}
$count += 1;
while ($count > 0) {
    $count--;
    print_r($arr[$count] . " : ");
}
echo '<hr>';

#do-while
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
foreach ($arr as $key => $value) {
    $count++;
}
$count += 1;
do {
    $count--;
    print_r($arr[$count] . " : ");
} while ($count > 0);
echo '<hr>';

# Дана строка let str = 'Hi I am ALex' развернуть строку в обратном направлении
#while
$str = 'Hi I am ALex';
$i = 0;
while ($str[$i]) {
    $i++;
    $arrA[] = $str[$i];
}
#foreach
$count = 0;
foreach ($arrA as $key => $value) {
    $count++;
}
#for
for (; $count >= 0; $count--) {
    echo $arrA[$count];
}
echo "<hr>";

#Дана строка. готовую функцию
#сделать ее с с маленьких букв
$str = 'Hi I am ALex';
echo $str = strtolower($str);
echo "<br>";
#сделать все буквы большие
echo $str = strtoupper($str);
echo "<hr>";

#Дана строка let str = 'Hi I am ALex' развернуть ее в обратном направлении
#while
$str = 'Hi I am ALex';
$i = 0;
while (isset($str[$i])) {
    $i++;
    $arrB[] = $str[$i];
}
#foreach
$count = 0;
foreach ($arrB as $key => $value) {
    $count++;
}
#for
for (; $count >= 0; $count--) {
    echo  $arrB[$count];
}
echo "<hr>";

# Дан массив сделать все буквы с маленькой
$arrC = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
foreach ($arrC as $value) {
    echo strtolower($value) . ', ';
}
echo "<hr>";
#Дан массив сделать все буквы с большой
$arrC = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
foreach ($arrC as $value) {
    echo strtoupper($value) . ', ';
}
echo "<hr>";
#Дано число развернуть ее в обратном направлении

$num = 1234678;
$str = (string)$num;
$i = 0;
while ($str[$i]) {
    $i++;
    $arrD[] = $str[$i - 1];
}
#foreach
$count = 0;
foreach ($arrD as $key => $value) {
    $count++;
}
#for
for (; $key >= 0; $key--) {
    echo  $arrD[$key];
}
echo "<hr>";

#Дан массив, отсортируй его в порядке убывания
#for + foreach
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
foreach ($arr as $key => $val) {
    $count++;
}
for ($a = $count; $a > 0; $a--) {
    for ($b = $count; $b > 0; $b--) {
        if ($arr[$b] > $arr[$b - 1]) {
            $temp = $arr[$b - 1];
            $arr[$b - 1] = $arr[$b];
            $arr[$b] = $temp;
        }
    }
}
echo "<pre>";
print_r($arr); 
echo "</pre>";
echo "<hr>";

